app.controller('loginController',function($scope,$rootScope,$http,$location){
	console.log("inside login controller");

	var captchaCode = "";
	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("LoginCaptcha").value = code

		captchaCode=code;
		console.log(captchaCode);
		//removing spaces from captcha code
		captchaCode = captchaCode.replace(/\s/g, '');
		console.log(captchaCode);
	}


	var getAlertsAndUpdate=function(){
		var res = $http.get('http://localhost:8080/getactivealerts');
		res.then(function(response){
			$scope.alertsAndUpdateDetails = response.data;
			console.log(response.data);
			console.log("status:" + response.status);
		});
	}

	getCaptcha();

	getAlertsAndUpdate();
	console.log($scope.displayedCaptchaCode);
	/**/

//	code to remove spaces :  str = str.replace(/\s/g, '');

	$scope.getCaptchaCode= function(){
		console.log("inside getCaptchaCode() func");
		console.log($scope.enteredCptchCode);	
	}
	$scope.loginUser=function(){
		console.log("inside login function");
		console.log(captchaCode);
		console.log($scope.enteredCptchCode);


		if(captchaCode != $scope.enteredCptchCode){
			location.reload();
			alert("invalid Captcha");
		}
		else{

			var userObject={
					userId:$scope.userId,
					userPassword:$scope.userPassword
			}
			console.log(userObject);
			var res = $http.post('http://localhost:8080/authuser',userObject);
			res.then(
					function mySuccess(response){
						$scope.user = response.data;
						$rootScope.currentUser=response.data;
						console.log(response.data);
						console.log("status:" + response.status);
						$location.path('/userHome/userService');						
					},
					function myError(response) {
						$scope.errormsg = "invalid userid and password";
					});
		}

	}

});

app.controller('forgetPasswordController',function($scope,$rootScope,$http,$location){
	console.log("inside forget password controller");
	$scope.basicUserdetails=true;
	$scope.userdetails=false;

	var captchaCode = "";
	var fetchedSecurityAnswer="";
	getCaptcha = function(){
		var a = Math.ceil(Math.random() * 9)+ '';
		var b = Math.ceil(Math.random() * 9)+ '';       
		var c = Math.ceil(Math.random() * 9)+ '';  
		var d = Math.ceil(Math.random() * 9)+ '';  
		var e = Math.ceil(Math.random() * 9)+ '';  
		var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
		document.getElementById("txtCaptcha").value = code;
		//for next page captcha code
		document.getElementById("txtCaptcha2").value = code

		captchaCode=code;
		console.log(captchaCode);
		//removing spaces from captcha code
		captchaCode = captchaCode.replace(/\s/g, '');
		console.log(captchaCode);
	}

	getCaptcha();

	$scope.fetchUserDetails=function(userId){
		console.log(captchaCode);
		console.log($scope.enteredCptchCode);
		if(captchaCode != $scope.enteredCptchCode){
			alert("invalid Captcha");
		}
		else{
			var res = $http.get('http://localhost:8080/getuserbyuserid/'+userId);
			res.then(
					function mySuccess(response){
						$rootScope.userDetail = response.data;
						fetchedSecurityAnswer = $scope.userDetail.securityAnswer;
						console.log($scope.userDetail);
						console.log(fetchedSecurityAnswer);
						console.log("status:" + response.status);
						$scope.basicUserdetails=false;
						$scope.userdetails=true;
						getCaptcha();
					},

					function myError(response) {
						$scope.message=response.data;
						console.log(response.data);
						$scope.errormsg = "User Id dosen't exist";
					});
		}
	}



	$scope.changePassword=function(){
		if(captchaCode != $scope.enteredCaptchCode2){
			alert("invalid Captcha");
		}
		else{

			if(fetchedSecurityAnswer == $scope.securityAnswer){
				if($scope.userPassword===$scope.confirmUserPassword){
					var userPwdDetails={
							udId:$rootScope.userDetail.udId,
							dateOfBirth:$rootScope.userDetail.dateOfBirth,
							emailId:$rootScope.userDetail.emailId,
							firstName:$rootScope.userDetail.firstName,
							gender:$rootScope.userDetail.gender,
							lastName:$rootScope.userDetail.lastName,
							mobileNumber:$rootScope.userDetail.mobileNumber,
							nationality:$rootScope.userDetail.nationality,
							securityQuestion:$rootScope.userDetail.securityQuestion,
							userId:$scope.userId,
							securityAnswer:$scope.securityAnswer,
							userPassword:$scope.userPassword,
					}

					console.log(userPwdDetails);
					var res = $http.put('http://localhost:8080/updateuser', userPwdDetails);
					res.then(
							function mySuccess(response) {
								console.log(response.data);
								//$scope.successMessage="Password Successfully Changed";
								$location.path('/passwordSuccessfullyChanged');

							},

							function myError(response) {
								$scope.message=response.data;
							});
				}
				else{
					alert("re-entered password does not matches with password!!!")
					$scope.message="ENTER password again";
				}
			}
			else{
				alert("Entered security answer does not matches with correct security answer!!!")
			}

		}

	}
});




/*
 * Controller for trs contacts
 */
app.controller('trsContactController',function($scope,$http,$location){
	console.log("inside trs contact controller");
	var getTrsContactInfo=function(){

		var res = $http.get('http://localhost:8080/getcontacts');
		res.then(function(response){
			$scope.trsContactDetails = response.data;
			console.log($scope.trsContactDetails);
			console.log("status:" + response.status);
		}); 
	}
	getTrsContactInfo();
});